
const { describe, it } = require('mocha'),
  sa = require('superagent'),
  CernAuth = require('../src');

describe.skip('CernAuth on Dashboards', function() {
  // not working for the moment, server kicks us out
  it('can use an agent with HTTP/2', async function() {
    this.timeout(30000); // timber is slow
    const agent = sa.agent();
    agent.use(new CernAuth(agent).plugin);

    await agent.get('https://controls-dashboards.cern.ch/api/cmw')
    .http2()
    .set({ 'Cache-Control': 'no-cache', Accept: 'text/event-stream' })
    .query({
      deviceName: 'TDI.4L2_Align',
      propertyName: 'AlignmentState',
      Selector: ''
    })
    .then((ret) => {
      return new Promise((resolve) => {
        ret.on('data', (d) => resolve(d));
        ret.destroy();
      });
    });
  });
});
