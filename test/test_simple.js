
const { describe, it } = require('mocha'),
  { expect } = require('chai'),
  sa = require('superagent'),
  CernAuth = require('../src'),
  { Writable } = require('stream');

describe('CernAuth', function() {

  it('can use an agent', async function() {
    const agent = sa.agent();
    agent.use(new CernAuth(agent).plugin);

    await agent.get('https://op-webtools.web.cern.ch/planning/#/');
    // will have a 301 redirect
    const ret = await agent.get('https://op-webtools.web.cern.ch/planning');
    expect(ret.body).to.deep.equal({});
  });

  it('can use its internal agent', async function() {
    const cernAuth = new CernAuth();

    await sa.get('https://op-webtools.web.cern.ch/planning')
    .use(cernAuth.plugin);
  });

  it('can pipe a request', async function() {
    const cernAuth = new CernAuth();

    return new Promise((resolve, reject) => {
      let message = false;
      sa.get('https://op-webtools.web.cern.ch/planning')
      .use(cernAuth.plugin)
      .pipe(new Writable({
        write(chunk, encoding, callback) {
          message = true;
          callback();
        },
        destroy() {
          if (message) {
            resolve();
          }
          else {
            reject('message not received');
          }
        }
      }));
    });
  });
});
