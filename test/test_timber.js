
const { describe, it } = require('mocha'),
  sa = require('superagent'),
  CernAuth = require('../src');

describe.skip('CernAuth on Timber', function() {

  it('can use an agent', async function() {
    this.timeout(30000); // timber is slow
    const agent = sa.agent();
    agent.use(new CernAuth(agent).plugin);

    await agent.post('https://timber.cern.ch/api/record-values')
    .timeout({ response: 5000, deadline: 30000 })
    .send({
      system: "CMW",
      variableName: "PR.TDI47:Acq:actualBeamIntensity",
      fundamentals: [],
      timeWindows: [ { startTime: "2021-02-23T11:26:18.960Z", endTime: "2021-02-23T12:26:18.960Z" } ],
      sampling: false,
      advanceOption: "TIMESCALE",
      scaleValue: 1,
      scaleInterval: "minutes",
      scaleAlgorithm: "MAX"
    });
  });

  it('can use its internal agent', async function() {
    this.timeout(30000); // timber is slow
    const cernAuth = new CernAuth();

    await sa.post('https://timber.cern.ch/api/record-values')
    .use(cernAuth.plugin)
    .timeout({ response: 5000, deadline: 30000 })
    .send({
      system: "CMW",
      variableName: "PR.TDI47:Acq:actualBeamIntensity",
      fundamentals: [],
      timeWindows: [ { startTime: "2021-02-23T11:26:18.960Z", endTime: "2021-02-23T12:26:18.960Z" } ],
      sampling: false,
      advanceOption: "TIMESCALE",
      scaleValue: 1,
      scaleInterval: "minutes",
      scaleAlgorithm: "MAX"
    });
  });
});
