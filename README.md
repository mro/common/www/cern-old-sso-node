
# CERN SSO (old) 

[SuperAgent](https://visionmedia.github.io/superagent/) plugin to authenticate old CERN SSO (login.cern.ch).

# Configuration

```bash
# Add CERN certificates
export NODE_EXTRA_CA_CERTS=/etc/ssl/certs/ca-bundle.crt

# Use an external keytab file
export KRB5_CLIENT_KTNAME=/etc/krb5/xxx.keytab
```

**Note:** It seemsYour machine **must** have a valid kerberos ticket and an hostname in  `cern.ch` domain.

## Using an external keytab file

```bash
export 
```

## Examples

```js
const
  CernAuth = require('@cern/old-sso-node'),
  sa = require('superagent');

/* using an agent */
const agent = sa.agent();
agent.use(new CernAuth(agent).plugin);

agent.get('https://op-webtools.web.cern.ch/planning')
.then((ret) => console.log(ret));

/* not using an agent */
const cernAuth = new CernAuth();

sa.get('https://op-webtools.web.cern.ch/planning')
.use(cernAuth.plugin)
.send({ /*...*/ })
.then((ret) => console.log(ret));
```
