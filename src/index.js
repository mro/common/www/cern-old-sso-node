// @ts-check

const
  { get, first, transform, startsWith } = require('lodash'),
  sa = require('superagent'),
  { URL } = require('url'),
  kerberos = require('kerberos').Kerberos,
  { DOMParser } = require('xmldom'),
  debug = require('debug')('auth');

class CernAuth {
  /**
   * @param {sa.SuperAgent<any>} [agent]
   */
  constructor(agent) {
    this.agent = agent || sa.agent();
    this.plugin = this._call.bind(this);
  }

  /**
   * @param  {sa.SuperAgent<any>} agent
   * @param  {string} redirect
   * @return {Promise<void>}
   */
  static async auth(agent, redirect) {
    const redirectUrl = new URL(redirect);
    redirectUrl.pathname += 'auth/integrated/';
    redirect = redirectUrl.toString();

    const krbClient = await kerberos.initializeClient("HTTP@login.cern.ch", {
      mechOID: kerberos.GSS_MECH_OID_SPNEGO
    });
    const ticket = await krbClient.step("");

    const ret = await agent.get(redirect)
    .set('Authorization', 'Negotiate ' + ticket);
    const parser = new DOMParser();
    const tree = parser.parseFromString(ret.text);
    const form = first(tree.getElementsByTagName('form'));
    if (!form) { throw new Error('[CernAuth]: login form not found'); }

    const action = form.getAttribute('action');
    const data = transform(form.getElementsByTagName('input'), (ret, input) => {
      if (input.getAttribute && input.getAttribute('name')) {
        ret[input.getAttribute('name')] = input.getAttribute('value');
      }
    }, /** @type {{ [key: string]: string }} */ ({}));
    if (!action) { throw new Error('[CernAuth]: login action not found'); }

    /* do not follow redirect, won't work for POST/PUT/DELETE methods */
    await agent.post(action).redirects(0)
    .type('form').send(data)
    .catch((/** @type {any} */ err) => {
      if (!(get(err, [ 'status' ]) === 302)) { throw err; }
    });
  }

  /**
   * @param {sa.SuperAgentRequest} request
   */
  _call(request) {
    // @ts-ignore: internal api
    const redirect = request._redirect;
    const pipe = request.pipe;
    const agent = this.agent;
    // @ts-ignore
    request.pipe = function(...args) {
      // save pipeline options
      this._pipeOpts = args;
      return pipe.apply(this, args);
    };
    // @ts-ignore: internal api
    request._redirect = /** @this {sa.SuperAgentRequest} */ function(/** @type {sa.Reponse} */ res) {
      const redir = get(res, [ 'headers', 'location' ]);
      if (redir && res.statusCode === 302 &&
          startsWith(redir, 'https://login.cern.ch')) {
        res.resume(); // terminate request
        debug('runing auth for %s', this.url);
        CernAuth.auth(agent, redir)
        .then(() => {
          // @ts-ignore: internal api
          agent._attachCookies(this);

          debug('authentication renewed');
          // @ts-ignore: internal api
          this._endCalled = false;

          // @ts-ignore: internal api
          this._retry();
          // restart pipeline
          // @ts-ignore: extension
          if (this._pipeOpts) {
            // @ts-ignore: internal api
            this._pipeContinue(...this._pipeOpts);
          }
          return this;
        }) // @ts-ignore of course it exists
        .catch((e) => this.callback(e, res));
      }
      else {
        return redirect.call(this, res);
      }
    };
    return this;
  }
}

module.exports = CernAuth;
